# TriLocate
This project aims to determine the location of Lora devices using the Lorawan triangulation principle. As the devices do not have GPS capabilities to provide live locations, the system relies on the time of arrival of data from the devices to the gateways for location identification. To facilitate this process, the gateways must support the finetimestamp functionality. The ChirpStack platform is integrated into the system to receive live data from the ChirpStack REST API service. If data from a device is received by more than two gateways, the system performs triangulation to pinpoint the device's location. The device data, along with location information, is stored in the database. Live device data is visualized on an OpenStreetMap for easy interpretation.

These devices will be deployed in buses for air quality monitoring purposes. By integrating this technology into buses, we aim to gather real-time data on air quality levels during transit. The implementation of the device in buses enhances our ability to monitor and assess air quality across different locations, providing valuable insights into environmental conditions. This initiative contributes to a broader understanding of air quality variations and supports efforts to create healthier and more sustainable transportation environments.

![HOME](Images/home.png)
![MAP](Images/map.png)
![ADD CHIRPSTACK](Images/add_chirpstack.png)
![ADD DEVICE](Images/add_device.png)
![ADD BUS STOPS](Images/add_bus_stops.png)



## Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/techworldthink/location_by_triangulation-lorawan.git
    ```

2. Install project dependencies:
    ```bash
    pip install -r requirements.txt
    ```

3. Configure your MySQL settings in `settings.py`.
    ```python
    # Development
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': BASE_DIR / 'db.sqlite3',
        }
    }
    ```
4. Create initial migrations
    ```bash
    python3 manage.py migrate
    ```
5. Apply the migrations and update the database
    ```bash
    python3 manage.py makemigrations
    ```
6. Create a superuser for the admin interface
    ```bash
    python3 manage.py createsuperuser
    ```   
7. Run the Django development server:
    ```bash
    python manage.py runserver
    ```
8. Run Background Task

## Access the project in your web browser:
Open your web browser and navigate to [http://127.0.0.1:8000/](http://127.0.0.1:8000/)
